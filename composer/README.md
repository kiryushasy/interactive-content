# Набор зависимостей для проекта, в котором:

### 1. Используется БД для хранения данных
[illuminate/database](https://packagist.org/packages/illuminate/database)

Пакет для работы с БД.


Создание объекта класса Capsule и инициализация этого объекта констанстами:

```php
//php code 
use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'database',
    'username' => 'root',
    'password' => 'password',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);

// Set the event dispatcher used by Eloquent models... (optional)
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
$capsule->setEventDispatcher(new Dispatcher(new Container));

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();
```


### 2. Используется кеш в памяти для временного хранения сложных запросов к БД
[watson/rememberable](https://packagist.org/packages/watson/rememberable)



Получить статьи первого пользователя и запомнить их на 6 часов :

```php
//php code 
User::first()->remember(360)->posts()->get();
```

### 3. Формируются XLS-отчеты на основе данных
[phpoffice/phpspreadsheet](https://packagist.org/packages/phpoffice/phpspreadsheet)
Библиотека предоставляющая набор классов для чтения и записи различных форматов файлов электронных таблиц.

### 4. Формируются PDF-документы на основе данных
[tecnickcom/tcpdf](https://packagist.org/packages/tecnickcom/tcpdf)
PHP библиотека для генерации PDF документов

### 5. Отправляются SMS-сообщения для верификации пользователей
[twilio/sdk](https://packagist.org/packages/twilio/sdk)

PHP обетка для API Twilio.

Отправка sms:

```php
    // php code
    // Required if your environment does not handle autoloading
    require __DIR__ . '/vendor/autoload.php';

    // Use the REST API Client to make requests to the Twilio REST API
    use Twilio\Rest\Client;

    // Your Account SID and Auth Token from twilio.com/console
    $sid = 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
    $token = 'your_auth_token';
    $client = new Client($sid, $token);

    // Use the client to do fun stuff like send text messages!
    $client->messages->create(
        // the number you'd like to send the message to
        '+15558675309',
        [
            // A Twilio phone number you purchased at twilio.com/console
            'from' => '+15017250604',
            // the body of the text message you'd like to send
            'body' => 'Hey Jenny! Good luck on the bar exam!'
        ]
    );
```

### 6. Отправляются E-mail-уведомления и рассылки для пользователей
[symfony/mailer](https://packagist.org/packages/symfony/mailer)

Библиотека для отправки электронных сообщений.

Пример отправки сообщения:

```php
use Symfony\Bridge\Twig\Mime\BodyRenderer;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Mailer\EventListener\MessageListener;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Twig\Environment as TwigEnvironment;

$twig = new TwigEnvironment(...);
$messageListener = new MessageListener(null, new BodyRenderer($twig));

$eventDispatcher = new EventDispatcher();
$eventDispatcher->addSubscriber($messageListener);

$transport = Transport::fromDsn('smtp://localhost', $eventDispatcher);
$mailer = new Mailer($transport, null, $eventDispatcher);

$email = (new TemplatedEmail())
    // ...
    ->htmlTemplate('emails/signup.html.twig')
    ->context([
        'expiration_date' => new \DateTime('+7 days'),
        'username' => 'foo',
    ])
;
```

### 7. Используется облачное хранилище AWS S3 или Windows Azure Blob для статичных файлов
[aws/aws-sdk-php](https://packagist.org/packages/aws/aws-sdk-php)

Пакет для создание php-приложений с использованием сервисов Amazon (Amazon S3, Amazon DynamoDB, Amazon Glacier)

```php
// php code
require 'vendor/autoload.php';
use Aws\S3\S3Client;
// Настройка клиента для S3, указание региона и последней версии
$s3 = new Aws\S3\S3Client([
    'version' => 'latest',
    'region'  => 'us-east-1'
]);
// Отправка запроса PutObject
$s3Client->putObject([
    'Bucket' => 'bucket',
    'Key'    => 'key',
    'Body'   => 'body'
]);
// Загрузка объекта
$result = $s3Client->getObject([
    'Bucket' => 'bucket',
    'Key'    => 'key'
]);
// Удаление контейнера
$client->deleteBucket([
    'Bucket' => 'test-bucket',
]);
```

### 8. Используется интеграция со службой доставки для расчета стоимости (например, PickPoint или Почта России)
[sch-group/pickpoint](https://packagist.org/packages/sch-group/pickpoint)
``` php
Инициализация

$config = [
 'host' => '',
 'login' => '',
 'password' => '',
 'ikn' => '',
];    

$pickPointConf = new PickPointConf($config['host'], $config['login'], $config['password'], $config['ikn']);

$defaultPackageSize = new PackageSize(20, 20,20); // может быть null

$senderDestination = new SenderDestination('Москва', 'Московская обл.'); // Адрес отправителя

$client = new PickPointConnector($pickPointConf, $senderDestination, $defaultPackageSize);
 
 
Так же можно добавить кэширование, для ускорения запроса на авторизацию 
$redisCacheConf = [
 'host' => '127.0.0.1',
 'port' => 6379
];

$client = new PickPointConnector($pickPointConf, $senderDestination, $defaultPackageSize, $redisCacheConf);
```



### 8. Дополинтельно для интеграции с Почтой России
[lapaygroup/russianpost](https://packagist.org/packages/lapaygroup/russianpost)
``` php 
try {
  $objectId = 2020; // Письмо с объявленной ценностью
  // Минимальный набор параметров для расчета стоимости отправления
  $params = [
              'weight' => 20, // Вес в граммах
              'sumoc' => 10000, // Сумма объявленной ценности в копейках
              'from' => 109012 // Почтовый индекс места отправления
              ];

  // Список ID дополнительных услуг 
  // 2 - Заказное уведомление о вручении 
  // 21 - СМС-уведомление о вручении
  $services = [2,21];

  $TariffCalculation = new \LapayGroup\RussianPost\TariffCalculation();
  $calcInfo = $TariffCalculation->calculate($objectId, $params, $services);
}

catch (\LapayGroup\RussianPost\Exceptions\RussianPostTarrificatorException $e) {
    // Обработка ошибок тарификатора 
    $errors = $e->getErrors(); // Массив вида [['msg' => 'текст ошибки', 'code' => код ошибки]]
}

catch (\LapayGroup\RussianPost\Exceptions\RussianPostException $e) {
    // Обработка ошибочного ответа от API ПРФ
}

catch (\Exception $e) {
    // Обработка нештатной ситуации
}
```
### 9. Используется интеграция с социальными сетями для авторизации пользователей
[hybridauth/hybridauth](https://packagist.org/packages/hybridauth/hybridauth)

PHP библиотека для интеграции с социальными сетями. Возможна авторизация на сойте через Facebook, Twitter и Google.

Аутентификация с помощью GitHub:

```php
// php code
include 'vendor/autoload.php';
$config = [
    'callback' => 'https://example.com/path/to/script.php',
    'keys' => [ 'id' => 'your-app-id', 'secret' => 'your-app-secret' ]
];
$github = new Hybridauth\Provider\GitHub($config);
$github->authenticate();
$userProfile = $github->getUserProfile();
```

### 10. Данные о товарах регулярно отправляются в Яндекс.Маркет
[yandex-market/yandex-market-php-partner](https://packagist.org/packages/yandex-market/yandex-market-php-partner)
Партнерский API Яндекс.Маркета
``` php
// Указываем авторизационные данные
$clientId = '9876543210fedcbaabcdef0123456789';
$token = '01234567-89ab-cdef-fedc-ba9876543210';

// Создаем экземпляр клиента с базовыми методами
$baseClient = new \Yandex\Market\Partner\Clients\BaseClient($clientId, $token);

// Магазины возвращаются постранично
$pageNumber = 0;
do {
    $pageNumber++;
    
    // Получаем страницу магазинов с номером pageNumber
    $campaignsObject = $baseClient->getCampaigns(['page' => $pageNumber,]);
    // Получаем итератор по магазинам на странице
    $campaignsPage = $campaignsObject->getCampaigns();

    // Получаем количество магазинов на странице
    $campaignsCount = $campaignsPage->count();

    // Получаем первый магазин
    $campaign = $campaignsPage->current();
    // Печатаем идентификатор и URL магазина, затем переходим к следующему    
    for ($i = 0; $i < $campaignsCount; $i++) {
        echo 'ID: ' . $campaign->getId();
        echo 'Domain: ' . $campaign->getDomain();        
        $campaign = $campaignsPage->next();
    }
    
    // Получаем информацию о страницах. Возвращаемое количество страниц может увеличиваться 
    // по мере увеличения номера страницы. Последняя страница будет достигнута, 
    // когда вернется количество страниц, равное номеру текущей страницы    
    $campaignsTotalPages = $campaignsObject->getPager()->getPagesCount();
} while ($pageNumber != $campaignsTotalPages); 

```

### 11. Принимается онлайн-оплата от покупателей
[trustly/trustly-client-php](https://packagist.org/packages/trustly/trustly-client-php)
Релизация онлайн-платежей.
``` php
require_once('Trustly.php');

/* Change 'test.trustly.com' to 'trustly.com' below to use the live environment */
$api = new Trustly_Api_Signed(
                $trustly_rsa_private_key,
                $trustly_username,
                $trustly_password,
                'test.trustly.com'
            );

$deposit = $api->deposit(
                "$base_url/php/example.php/notification",   /* NotificationURL */
                'john.doe@example.com',                     /* EndUserID */
                $messageid,                                 /* MessageID */
                'en_US',                                    /* Locale */
                $amount,                                    /* Amount */
                $currency,                                  /* Currency */
                'SE',                                       /* Country */
                NULL,                                       /* MobilePhone */
                'Sam',                                      /* FirstName */
                'Trautman',                                 /* LastName */
                NULL,                                       /* NationalIdentificationNumber */
                NULL,                                       /* ShopperStatement */
                $ip,                                        /* IP */
                "$base_url/success.html",                   /* SuccessURL */
                "$base_url/fail.html",                      /* FailURL */
                NULL,                                       /* TemplateURL */
                NULL,                                       /* URLTarget */
                NULL,                                       /* SuggestedMinAmount */
                NULL,                                       /* SuggestedMaxAmount */
                'trustly-client-php example/1.0'            /* IntegrationModule */
                FALSE,                                      /* HoldNotifications */
                'john.doe@example.com',                     /* Email */
                'SE',                                       /* ShippingAddressCountry */
                '12345',                                    /* ShippingAddressPostalCode */
                'ExampleCity',                              /* ShippingAddressCity */
                '123 Main St'                               /* ShippingAddressLine1 */
                'C/O Careholder',                           /* ShippingAddressLine2 */
                NULL                                        /* ShippingAddress */
            );

$iframe_url= $deposit->getData('url');

Example notification processing
$request = $api->handleNotification($notification_body);
    # FIXME Handle the incoming notification data here
$notifyresponse = $api->notificationResponse($request, TRUE);

echo $notifyresponse->json();

```

### 12. Применяются средства тестирования (например, PHPUnit)
[phpunit/phpunit](https://packagist.org/packages/phpunit/phpunit)
PHP фреймворк для модульного тестирования.

```php
use PHPUnit\Framework\TestCase;
class StackTest extends TestCase
{
  public function testPushAndPop()
  {
    $stack = [];
    $this->assertSame(0, count($stack));
    array_push($stack, 'foo');
    $this->assertSame('foo', $stack[count($stack)-1]);
    $this->assertSame(1, count($stack));
    $this->assertSame('foo', array_pop($stack));
    $this->assertSame(0, count($stack));
  }
}
```
